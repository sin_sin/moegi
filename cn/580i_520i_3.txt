// 集結。

#center,0.5,kana_inner_002.png,0
$voice,52001402
華奈「お土産は何をお願いしよっか？」
$k

場の空気を変えるように、華奈姉さんは明る
い声を上げた。
$k

大悟「気が早いなあ、まだ本人が出発しても
　いないのに」
$k

#center,0.5,kana_inner_005.png,0
$voice,52001502
華奈「いいのっ！　こういうのは出発する前
　に言っておくの！」
$k

大悟「…はいはい」
$k

#center,0.5,kana_inner_004.png,0
$voice,52001602
華奈「う〜ん、ドイツって何があるのかなあ
　？　バウムクーヘンとか、ソーセージとか
　、ビールとか……」
$k

大悟「華奈姉さんは、ビールは飲まないでし
　ょ？」
$k

#center,0.5,kana_inner_003.png,0
$voice,52001702
華奈「うん、あの苦いのがあんまり好きじゃ
　ないんだけど、でもドイツの人はお水代わ
　りにビールをがぶがぶ飲むんだよ？」
$k

#center,0.5,kana_inner_002.png,0
$voice,52001802
華奈「だから、ドイツのビールは苦くないん
　じゃないのかなあ。それなら私も好きにな
　れるかも」
$k

大悟「…いや、ビールはビールだよ、きっと
　」
$k

#center,0.5,kana_inner_001.png,0
…と、そんなことを話しながら静かな夜の道
を歩いた。
$k

華奈姉さんが割と『大人』な考え方をしてい
たのは意外だと思う反面、良かったとも思う
。
$k

きっと彼女のことだから、みちる先生に泣き
ついて駄々をこねて、その意志を大いにぐら
つかせるだろうと思っていたが、
$k

…でも、華奈姉さんだって一人の教師なんだ
。みちる先生にとって何が大切かは、よくわ
かっているのだろう。
$k

そんな華奈姉さんの姿勢を、自分も見習わな
ければならないと思う。
$k

……。
$k

// ＢＧＭストップ。
$bgm,stop,YES

// 全面黒。
#background,1.0,black.jpg,same,0

…しかし。
$k

華奈姉さんとは対照的に、『彼女』は全く違
う反応を示した。
$k

初めて『感情的になった』とでも言うのだろ
うか。彼女が意外なほど強い自我を持ってい
たことに、大いに驚かされた。
$k

それは、翌日の昼休みのことだった。
$k

……。
$k

#hide,0.5

// 次のファイルへ。
$next,521i_0711_yui
