// 415.txt
// 　　ファイルナンバー４１５　・・・　歩美と一緒に過ごす夜
// 　　　　　　登場　・・・　５歩美
// 　　　　７月８日（日）　午後１１時００分　自分の部屋
#screen,1.0,eye_background.jpg,eye_day_0708.png,eye_time_pm11.png,eye_loc_jibun_heya.png,eye_min_00.png,0
$k

// ＢＧＭスタート。
$bgm,15,YES

// 自分の部屋（夜）。歩美パジャマ。
#background,1.0,jibun_heya_yoru.jpg,erase,0
#center,0.5,ayumi_nemaki_141.png,0
#show,0.5

$voice,41500005
歩美「……」
$k

大悟「……」
$k

$voice,41500105
歩美「……」
$k

大悟「……」
$k

…全く会話がなかった。
$k

ただお互いに視線を逸らし、真っ赤になり、
その胸をドキドキさせていた。
$k

夕食前に言われたみちる先生からの提案…、
いや正確には、逆らうことの許されない厳格
な命令。
$k

居間のソファーでもなく、他の三人の部屋で
もなく、この家の中で歩美ちゃんが寝室とし
て使うことのできる唯一の場所。
$k

それは……、
$k

…もう説明の必要はないだろう。今、目の前
にあるこの光景がそれである。
$k

#center,0.5,ayumi_nemaki_176.png,0
$voice,41500205
歩美「ふ…、ふつつか者ですが、よろしくお
　願いします！」
$k

突然、ものすごい勢いで三つ指を突く歩美ち
ゃん。
$k

大悟「こ…、こちらこそ、よろしくお願いし
　ますっ！」
$k

俺も全く同じ仕草で返す。気負い、緊張、興
奮……、それらも全く同じままに。
$k

#center,0.5,ayumi_nemaki_141.png,0
$voice,41500305
歩美「……」
$k

大悟「……」
$k

…そして再び静寂に包まれる空間。お互いに
心臓の鼓動の音さえも聞き取れそうなくらい
だった。
$k

大悟「え〜っと…、その……」
$k

大悟「歩美…、ちゃん…？」
$k

#center,0.5,ayumi_nemaki_150.png,0
$voice,41500405
歩美「は…、はいっ！？」
$k

何とか緊張を緩めようと声を掛けてみても、
むしろそれは逆効果。歩美ちゃんは驚いたよ
うに声を上げてしまう。
$k

…が、このままでは何もできないので、彼女
にはかわいそうだが話を進めさせてもらおう
。
$k

大悟「ベッド、使っていいからさ。俺は適当
　に床で寝るよ」
$k

#center,0.5,ayumi_nemaki_141.png,0
$voice,41500505
歩美「そんな…、それじゃあ大悟君が……」
$k

大悟「大丈夫大丈夫。夏だから風邪なんて引
　かないって」
$k

$voice,41500605
歩美「……」
$k

無論、そんなことが問題ではないのだが、『
反論できない理由』としてはそれが一番良い
。
$k

大悟「シーツも洗い立てのに取り替えといた
　から。男臭い匂いもしないと思うよ？」
$k

大悟「…それとも、歩美ちゃんは布団派だっ
　たかな？　それじゃあ、ちょっと寝づらい
　かも」
$k

大悟「あははは……」
$k

#center,0.5,ayumi_nemaki_162.png,0
$voice,41500705
歩美「……」
$k

#center,0.5,ayumi_nemaki_150.png,0
$voice,41500805
歩美「…ごめんね、大悟君」
$k

大悟「えっ？」
$k

場を和ませるようにわざとらしく上げた高笑
いだったが、逆に歩美ちゃんはしんみりとし
た声と表情で応えた。
$k

#center,0.5,ayumi_nemaki_141.png,0
$voice,41500905
歩美「私…、どうかしてたんだと思うわ。あ
　なたと同棲したいなんて」
$k

$voice,41501005
歩美「実際に来てみたら、あなたに迷惑を掛
　けることばかりで…。ベッドだけじゃなく
　、ご飯だって、お風呂だって……」
$k

大悟「い…、いいんだよ、そんなこと。ここ
　には元々居候が三人もいたんだから、今さ
　ら一人や二人増えたって変わらないよ」
$k

#center,0.5,ayumi_nemaki_162.png,0
$voice,41501105
歩美「本当に…、ごめんなさい……」
$k

大悟「歩美ちゃん……」
$k

俺のフォローも通じず、なおも沈んだ面持ち
となる彼女。
$k

確かに自分も最初は驚いたが、今は前向きに
捉えている…。それだけは彼女にもわかって
ほしいのだが。
$k

大悟「ここ…、座りなよ」
$k

#center,0.5,ayumi_nemaki_004.png,0
$voice,41501205
歩美「えっ…？」
$k

俺はベッドの上をぽんぽんと叩いて、彼女を
促した。
$k

大悟「『ふかふか』とまでは行かないけど、
　そんじょそこらの安物とは違うと思うよ？
　」
$k

#center,0.5,ayumi_nemaki_003.png,0
$voice,41501305
歩美「う…、うん……」
$k

歩美ちゃんは遠慮がちに頷きながら、ベッド
の縁に……、つまり俺の隣に腰掛けた。
$k

…少し大胆だっただろうか。この位置関係な
ら、普通の女の子は警戒心を抱いてしまう。
$k

大悟「いいんじゃない？　こういう『お泊ま
　り会』も」
$k

#center,0.5,ayumi_nemaki_004.png,0
$voice,41501405
歩美「『お泊まり会』…？」
$k

大悟「小さい頃……、幼稚園くらいの頃にさ
　、華奈姉さんの部屋に泊めてもらったこと
　があるよ」
$k

大悟「ベッドの中で絵本を読んでもらったり
　、折り紙を教えてもらったり……」
$k

大悟「…何だかいつもと違う雰囲気が、すご
　く楽しかったのを覚えてる」
$k

#center,0.5,ayumi_nemaki_141.png,0
$voice,41501505
歩美「……」
$k

大悟「歩美ちゃん、言ってたよね？　俺と幼
　なじみになりたい、って」
$k

大悟「まあ、それくらいの気楽さでさ、しば
　らくここで遊んでいくと良いと思うよ」
$k

大悟「ははは…」
$k

#center,0.5,ayumi_nemaki_162.png,0
$voice,41501605
歩美「……」
$k

#center,0.5,erase,0
…本音はちょっと違う。
$k

俺達は『幼なじみ』と呼べる年齢ではないし
、『お泊まり会』が気軽にできる関係でもな
い。
$k

それに…、そんな話をしながらも、この胸の
奥にはチクリと痛むものがある。
$k

…だが、彼女が望んでいるものはそれなんだ
。
$k

今の歩美ちゃんが願っているのは、ありふれ
た『幼なじみ』の関係なんだ。
$k

ならば…、俺はそれに応えなければならない
し、決して彼女を落胆させてはならないと思
う。
$k

#center,0.5,ayumi_nemaki_003.png,0
$voice,41501705
歩美「大悟君……」
$k

大悟「ん？」
$k

俺の思いが伝わったのか、歩美ちゃんはそっ
と目を上げた。
$k

#center,0.5,ayumi_nemaki_021.png,0
$voice,41501805
歩美「優しいのね、大悟君って…」
$k

大悟「そ…、そうかな…？」
$k

僅かに触れる彼女の髪。天の川のような美し
さ。
$k

#center,0.5,ayumi_nemaki_001.png,0
$voice,41501905
歩美「ベッド…、借りていい…？」
$k

…その言葉は、彼女が安堵した証拠なのだと
思う。
$k

大悟「うん、いいよ」
$k

当然、俺が頷かないわけがない。元々は俺か
ら言い出したのだから。
$k

大悟「それじゃあ、俺は毛布を出して、…っ
　と」
$k

#center,0.5,ayumi_nemaki_004.png,0
$voice,41502005
歩美「あ…、ちょっと待って」
$k

大悟「えっ？」
$k

膝をぱんと叩いて勢い良く立ち上がったとこ
ろで、不意に彼女から制止の声が掛かる。
$k

#center,0.5,ayumi_nemaki_003.png,0
$voice,41502105
歩美「詰めれば、大丈夫だと思うから」
$k

大悟「何が？」
$k

#center,0.5,ayumi_nemaki_162.png,0
$voice,41502205
歩美「だから…、ベッド……」
$k

大悟「……」
$k

#center,0.5,ayumi_nemaki_141.png,0
目をうるうるとさせて俺を見詰める歩美ちゃ
ん。しかもその頬は、艶やかかつ初々しい紅
色に染まっている。
$k

…だから、彼女の言いたいことは瞬時に理解
できた。その光景も鮮やかに脳裏に浮かんで
きた。
$k

でも、そんなことをしてしまったら、俺には
理性を保つ自信が全然なくて…、
$k

本能のままに行動してしまいそうで……。
$k

……。
$k

大悟「…暑いからさ、今夜は」
$k

#center,0.5,ayumi_nemaki_004.png,0
$voice,41502305
歩美「えっ…？」
$k

大悟「だいぶ寝苦しくなっちゃうと思うよ？
　　お互いに」
$k

#center,0.5,ayumi_nemaki_003.png,0
$voice,41502405
歩美「……」
$k

…紙一重の差で理性が勝った。
$k

心の中でさめざめと涙を流しながらも、それ
を表には一切出さなかった。
$k

大悟「ま…、まあ、毛布を何枚も重ねればや
　わらかくなるからさ。これにシーツを掛け
　れば、敷き布団の出来上がりっと！」
$k

大悟「夏だからこの程度で十分だよ！　さあ
　、早く寝よ寝よ、明日は学校だ！」
$k

大悟「お休みっ！」
$k

#center,0.5,ayumi_nemaki_004.png,0
$voice,41502505
歩美「あ…、大悟君っ！」
$k

#center,0.5,erase,0
俺は彼女に物すら言わせぬ勢いで即席布団を
用意し、その上に寝転んだ。
$k

歩美ちゃんの気持ちはうれしい。とてもうれ
しいけれど、だからこそ耐えなきゃならない
こともあるんだ。
$k

それを理解しろ、理解してくれ、俺の本能よ
…。
$k

……。
$k

// ＢＧＭストップ。
$bgm,stop,YES

$voice,41502605
歩美「……」
$k

$voice,41502705
歩美「……、…ふぅ」
$k

敢えて顔を背けているベッドの側から、小さ
く息をつく音が聞こえてくる。
$k

$voice,41502805
歩美「昨日の今日、…だもんね」
$k

大悟「……」
$k

$voice,41502905
歩美「…お休み、大悟君」
$k

その後は、…俺もこの週末の出来事に疲れて
いたのか、すぐ深い眠りへと落ちていった。
$k

……。
$k

#hide,0.5

// 次のファイルへ。
$next,416i_0709_yui
