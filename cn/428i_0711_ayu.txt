// 428.txt
// 　　ファイルナンバー４２８　・・・　それから数日間
// 　　　　　　登場　・・・　２華奈、３みちる、４優衣、５歩美
// 　　　　（日付表示なし。実際には７月１１日〜１３日）
#screen,1.0,eye_background.jpg,same,same,same,same,0
$k

// 日付設定。
$pfinto,1,7
$pfinto,2,11

// ＢＧＭスタート。
$bgm,17,YES

// 自宅の食卓（昼）
#background,1.0,jitaku_shokutaku_hiru.jpg,same,0
#show,0.5

それから後も、歩美ちゃんの甲斐甲斐しい働
きぶりとみちる先生のいやらしいイタズラは
続いた。
$k

#center,0.5,ayumi_inner_002.png,0
$voice,42800005
歩美「お夕飯ができました〜っ！」
$k

// 優衣室内着（左）。華奈室内着（右）。
#character,0.5,yui_inner_001.png,erase,kana_inner_022.png,0
$voice,42800102
華奈「は〜〜〜いっ！」
$k

$voice,42800204
優衣「はい」
$k

#character,0.5,erase,michiru_inner_001.png,erase,0
$voice,42800303
みちる「おう、今日もなかなか豪勢だな」
$k

#center,0.5,erase,0
大悟「…って、あれ？」
$k

#center,0.5,ayumi_inner_003.png,0
$voice,42800405
歩美「どうしたの？　大悟君」
$k

大悟「俺の箸がない……」
$k

#center,0.5,ayumi_inner_004.png,0
$voice,42800505
歩美「えっ？　さっき出したはずだけど……
　」
$k

// みちる室内着（左）。歩美室内着（右）。
#character,0.5,michiru_inner_006.png,erase,ayumi_inner_003.png,0
$voice,42800603
みちる「なにーっ！　大悟の箸がないだとー
　ーーっ！」
$k

$voice,42800703
みちる「家主の箸を出さないとはどういうこ
　とだ、若槻っ！　罰として、大悟に全て口
　移しで食わせることを命じるっ！」
$k

#right,0.5,ayumi_inner_004.png,0
$voice,42800805
歩美「そ…、そんな…っ！」
$k

#right,0.5,kana_inner_004.png,0
$voice,42800902
華奈「あれ〜？　みっちゃん、どうしてだい
　ちゃんのお箸を持ってるの〜？」
$k

#left,0.5,michiru_inner_162.png,0
$voice,42801003
みちる「…こういう時ばかり目敏くなるんじ
　ゃない、華奈」
$k

大悟「早く返してくださいよ、みちる先生」
　
$k

// 全面白ワイプ。
#background,1.0,white.jpg,same,0

// 自宅の居間（夜）ワイプ。歩美室内着。
#background,1.0,jitaku_ima_yoru.jpg,same,0
#center,0.5,ayumi_inner_002.png,0
$voice,42801105
歩美「お風呂がわきました〜っ！」
$k

// 優衣室内着（左）。華奈室内着（右）。
#character,0.5,yui_inner_001.png,erase,kana_inner_022.png,0
$voice,42801202
華奈「は〜〜〜いっ！」
$k

$voice,42801304
優衣「はい」
$k

#character,0.5,erase,michiru_inner_002.png,erase,0
$voice,42801403
みちる「よし、今日はあみだくじで一緒に風
　呂に入るペアを決めよう！」
$k

#center,0.5,michiru_inner_001.png,0
$voice,42801503
みちる「大悟はここ、若槻はここ！　華奈と
　優衣は残りの三ヶ所から好きなところを選
　べ！」
$k

大悟「…どうして俺と歩美ちゃんだけ、すで
　にスタート地点が決まってるんですか？」
$k

#center,0.5,michiru_inner_024.png,0
$voice,42801603
みちる「それが運命だからだっ！」
$k

大悟「…どうしてペアで風呂に入らなければ
　ならないんですか？」
$k

$voice,42801703
みちる「それも運命だからだっ！」
$k

大悟「……」
$k

// 優衣室内着（左）。華奈室内着（右）。
#character,0.5,yui_inner_001.png,erase,kana_inner_002.png,0
$voice,42801802
華奈「じゃあ、私はここ〜」
$k

$voice,42801904
優衣「私はここにします」
$k

#right,0.5,kana_inner_001.png,0
$voice,42802002
華奈「スタート地点を決めたらね、優衣ちゃ
　ん、こうやって線をいっぱい引くんだよ〜
　」
$k

#left,0.5,yui_inner_004.png,0
$voice,42802104
優衣「これがシャッフル代わりなんですね」
$k

#character,0.5,erase,michiru_inner_004.png,erase,0
$voice,42802203
みちる「ああ〜っ、勝手に線を引くな、二人
　とも！」
$k

大悟「残念ながら、みちる先生の野望はあっ
　さり崩れ去ってしまいましたね」
$k

#center,0.5,erase,0
…と、こんな感じでドタバタと時が過ぎてい
った。
$k

しかし、そんな中にあっても歩美ちゃんの疲
れは溜まっていたようで……。
$k

// 全面白ワイプ。
#background,1.0,white.jpg,same,0

// 教室（昼）ワイプ。歩美制服。
#background,1.0,gakuen_kyoushitsu_hiru.jpg,same,0

$voice,42802305
歩美「くぅ〜…、くぅ〜……」
$k

大悟「あ…、また寝てる……」
$k

#center,0.5,kana_suit_001.png,0
$voice,42802402
華奈「ここでオリヴァーと出会ったロザリン
　ドは、オーランドーが大ケガを負ったこと
　を知り、気を失ってしまいます」
$k

#center,0.5,kana_suit_004.png,0
$voice,42802502
華奈「…って、あら？」
$k

$voice,42400405
歩美「くぅ〜〜…、くぅ〜〜……」
$k

大悟「やばい、今日は間に合わなかった…！
　」
$k

#center,0.5,kana_suit_003.png,0
$voice,42802702
華奈「大丈夫？　歩美ちゃん……」
$k

#character,0.5,kana_suit_003.png,erase,ayumi_seifuku_141.png,0
$voice,42802805
歩美「……、…えっ？」
$k

#left,0.5,kana_suit_004.png,0
$voice,42802902
華奈「具合が悪かったら、保健室に行っても
　いいんだよ？」
$k

#right,0.5,ayumi_seifuku_004.png,0
$voice,42803005
歩美「あ…、い、いえ……、その……、す、
　すいませんでしたっ！」
$k

#left,0.5,kana_suit_001.png,0
$voice,42803102
華奈「ううん、謝ることないの。ただ、歩美
　ちゃんがつらそうだったから」
$k

#right,0.5,ayumi_seifuku_141.png,0
$voice,42803205
歩美「華奈先生……」
$k

#left,0.5,kana_suit_004.png,0
$voice,42803302
華奈「本当に大丈夫？」
$k

#right,0.5,ayumi_seifuku_162.png,0
$voice,42803405
歩美「は…、はい、大丈夫です……」
$k

#left,0.5,kana_suit_021.png,0
$voice,42803502
華奈「じゃあ、授業続けるね」
$k

大悟「……、…ふぅ」
$k

#character,0.5,erase,same,erase,0
華奈姉さんが穏やかに対応してくれたおかげ
で、クラスの中がざわつくようなことはなか
ったが、
$k

…さすがに他の先生だったらまずかったな。
歩美ちゃんのイメージががた落ちだ。
$k

やっぱり、何とかしないといけないな……。
$k

// 全面白。
#background,1.0,white.jpg,same,0

…そして、週の終わりの金曜日を迎える。
$k

彼女は努めて明るく振る舞いながらも、その
疲労はなかなか隠し切れるものではなかった
。
$k

……。
$k

#hide,0.5

// ＢＧＭストップ。
$bgm,stop,YES

// 次のファイルへ。
$next,429i_0713_ayu
