// 225.txt
// 　　ファイルナンバー２２５　・・・　火曜日の作業が終了
// 　　　　　　登場　・・・　２华奈、３美知留、４优衣、５步美
// 　　　　７月３日（火）　午後６時３０分　被服室
#screen,1.0,eye_background.jpg,eye_day_0703.png,eye_time_pm06.png,eye_loc_gakuen_hifukushitsu.png,eye_min_30.png,0
$k

// ＢＧＭスタート。
$bgm,09,YES

// 被服室（夜）
#background,1.0,gakuen_hifukushitsu_yoru.jpg,erase,0
#show,0.5

// 効果音：４２００：チャイム
$se,00004200,NO
（叮咚铛咚）
$k

// 优衣制服（左）。步美制服（右）。
#character,0.5,yui_seifuku_003.png,same,ayumi_seifuku_001.png,0
$voice,22500005
步美「今天就到此为止吧」
$k

#left,0.5,yui_seifuku_169.png,0
优衣「（点头）」
$k

大悟「是〜」
$k

#left,0.5,yui_seifuku_003.png,0
离校时间的铃声响起的同时宣告着今天工作的结束。
$k

进行状况十分良好，按照这个进度，完全可以如小步美所计划的完成。
$k

#right,0.5,ayumi_seifuku_002.png,0
$voice,22500205
步美「那么，拜托收拾一下哦」
$k

#character,0.5,erase,same,erase,0
…于是，各自进行着分配到的工作。
$k

收拾也不需要花多少时间，有5分钟的话，室内也能回复工作前的状态。
$k

// 优衣制服（左）。步美制服（右）。
#character,0.5,yui_seifuku_003.png,same,ayumi_seifuku_001.png,0
$voice,22500305
步美「辛苦了，明天也请多指教咯」
$k

#left,0.5,yui_seifuku_169.png,0
优衣「（点头）」
$k

大悟「辛苦了，小步美」
$k

互相寒暄之后，大家和和气气的解散了。
$k

……。
$k

// ＢＧＭストップ。
$bgm,stop,YES

// ＢＧＭスタート。
$bgm,03,YES

// 保健室（夜）ワイプ。美知留白衣（左）。华奈スーツ（右）。
#background,1.0,gakuen_hokenshitsu_yoru.jpg,same,0
#character,0.5,michiru_hakui_141.png,same,kana_suit_141.png,0

$voice,22500502
华奈「好〜无聊〜〜〜」
$k

$voice,22500603
美知留「啊〜〜〜没劲〜〜〜」
$k

大悟「……」
$k

…和被服室轻快的气氛形成鲜明的对比，这间保健室被沉重的气压所压制。
$k

#character,0.5,erase,kana_suit_003.png,erase,0
$voice,22500702
华奈「我要是不用工作的话，就能和小优一起做裁缝了啊〜」
$k

和昨天一样，华奈姐对这我大吐苦水。
$k

大悟「就算这么说…华奈姐也当不了裁缝的对不对？」
$k

#center,0.5,kana_suit_004.png,0
$voice,22500802
华奈「用缝纫机缝扣子还是会的呀。购物节目也做过这种示范的」
$k

$voice,22500902
华奈「最近的缝纫机好厉害呢。装载了电脑只要是喜欢的都能做呢」
$k

大悟「……」
$k

…果然华奈姐的生活能力为0。
$k

如果给小步美帮倒忙的话，反而不能按预定完成工作的吧。
$k

#center,0.5,michiru_hakui_141.png,0
$voice,22501003
美知留「不…不妙啊，大悟……」
$k

忽然转过眼去，就看到美知留老师捂着胸口作垂死状。
$k

#center,0.5,michiru_hakui_150.png,0
$voice,22501103
美知留「工作做过头了要死人了…。我大概已经…不行了……」
$k

#center,0.5,michiru_hakui_162.png,0
$voice,22501203
美知留「你要是能留在保健室，给我帮忙的话……这条命被蚕食的可能性…应该不存在的……」
$k

大悟「…大概，我想今天做的分量是一般保健老师的工作量哦？」
$k

#center,0.5,michiru_hakui_006.png,0
$voice,22501303
美知留「切，稍微看不见人就开始神气起来了嘛，年轻人」
$k

看我一副懒得搭理的态度，美知留老师无聊的咂了咂嘴。
$k

#center,0.5,michiru_hakui_003.png,0
$voice,22501403
美知留「…不过，工作过度是真的。因为明天，要出差呐」
$k

大悟「啊…确实这么说过了呢」
$k

加上日常业务（平常是我在做的部分），在自己不在的时间能让其他教师进行紧急对应的准备工作吧。
$k

这方面的勤勉，确实能部分尊敬一下。
$k

// 美知留白衣（左）。华奈スーツ（右）。
#character,0.5,michiru_hakui_003.png,erase,kana_suit_022.png,0
$voice,22501502
华奈「小美〜，特产〜，特〜产〜〜♪」
$k

一听到『出差』，华奈姐就开新的蹭了过来。
$k

#left,0.5,michiru_hakui_004.png,0
$voice,22501603
美知留「嗯〜…也不是去太远的地方，特产之类的虽然不会有……」
$k

#left,0.5,michiru_hakui_001.png,0
$voice,22502203
美知留「…嘛，适当的买一些点心好了，反正其他家伙的分也是必要的呢」
$k

#right,0.5,kana_suit_002.png,0
$voice,22502302
华奈「哇〜，好诶〜！」
$k

#character,0.5,erase,michiru_hakui_002.png,erase,0
$voice,22502403
美知留「所以说，把钱交出来，大悟」
$k

大悟「我的钱吗！」
$k

#center,0.5,michiru_hakui_003.png,0
$voice,22502503
美知留「不是我，是华奈想要。那样的话你总不能拒绝的吧」
$k

大悟「…那就是，反正除了威胁我别无他意的样子咯」
$k

她不论是什么样的说辞不够都是死乞白赖的讨钱而已，这种单纯的胆子早就明白了。
$k

……。
$k

// 全面白ワイプ。
#background,1.0,white.jpg,same,0

// 保健室（夜）ワイプ。美知留白衣（左）。华奈スーツ（右）。
#background,1.0,gakuen_hokenshitsu_yoru.jpg,same,0
#character,0.5,michiru_hakui_001.png,same,kana_suit_001.png,0
$voice,22502603
美知留「…所以说，明天就拜托了啊」
$k

大悟「是是」
$k

总之美知留老师像是进行『总结』一般，认真地对着我说出了这番话生。
$k

$voice,22502703
美知留「华奈也是，就靠你了哟」
$k

#right,0.5,kana_suit_022.png,0
$voice,22502802
华奈「好〜！」
$k

大悟「诶？ 华奈姐拜托她什么了么？」
$k

#right,0.5,kana_suit_002.png,0
$voice,22502902
华奈「嗯，明天的授课不多，所以下课后就来保健室工作」
$k

大悟「……」
$k

不由得感到不安……。
$k

#left,0.5,michiru_hakui_026.png,0
$voice,22503003
美知留「嘛、嘛…接待来客这种程度谁都能行的啦」
$k

…美知留老师和我貌似想的是同一件事。她难得的声音发虚还拉着一张脸。
$k

理解华奈姐的信赖，而且难得的厚意怎敢轻言拒绝……就是这样的吧。
$k

#left,0.5,michiru_hakui_005.png,0
$voice,22503103
美知留「（…没关系，没关系。姑且还是有所对策的）」
$k

是想对着我打信号吗，还是说给自己听的呢。感觉美知留老师在嘀咕着什么。
$k

#character,0.5,erase,michiru_hakui_001.png,erase,0
$voice,22503203
美知留「好了，那就回去吧」
$k

#center,0.5,erase,0
脱下白衣，美知留老师拿起了自己的包包。
$k

#center,0.5,yui_seifuku_001.png,0
优衣「……」
$k

同时，小优也啪的站起身来。
$k

#center,0.5,michiru_suit_001.png,0
大悟「啊…我还要到商店街购物，请把钥匙带上」
$k

#center,0.5,michiru_suit_002.png,0
$voice,22503403
美知留「哦，谢了」
$k

从包里拿出家中的钥匙，交给了美知留老师。
$k

#center,0.5,kana_suit_022.png,0
$voice,22503502
华奈「我要和小悟一起♪」
$k

华奈姐心情很好的站在我旁边。
$k

// 美知留スーツ（左）。优衣制服（右）。
#character,0.5,michiru_suit_001.png,erase,yui_seifuku_003.png,0
$voice,22503603
美知留「我和优衣先回去，在那里无事可做滚来滚去的看电视才是最好的呐」
$k

大悟「如果能准备洗澡水的话会比较高兴的……」
$k

#right,0.5,yui_seifuku_169.png,0
优衣「（点头）」
$k

和与平时一副德性的美知留老师相反，小优很老实的点了点头，这份温柔真是让人高兴。
$k

……。
$k

#hide,0.5

// ＢＧＭストップ。
$bgm,stop,YES

// 次のファイルへ。
$next,226i_0703_ka
