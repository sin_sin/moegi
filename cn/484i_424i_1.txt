// 選択肢１を選んだ場合。（メールを送ってみる。）
// 歩美好感度（＋１）
$pfplus,50,1

大悟「…やったほうがいいだろうな」
$k

それは歩美ちゃんのためを思うと同時に、華
奈姉さんに対する配慮でもある。
$k

華奈姉さんの授業は居眠りをする者が一人も
いないことが自慢だし、それに親衛隊のみな
さんも目を光らせているのだから。
$k

#center,0.5,kana_suit_001.png,0
$voice,42400602
華奈「次に、この例文ですが……」
$k

#center,0.5,erase,0
大悟「今だっ…！」
$k

俺は華奈姉さんが黒板側を向いた隙に、速攻
で歩美ちゃんにメールを出した。
$k

// 効果音：４６１２：携帯バイブレーション
$se,00004612,NO
（ぶるぶるっ、ぶるぶるっ）
$k

…と、彼女の携帯電話が振動していることだ
ろう。無論、その音は聞こえるはずがないけ
れども。
$k

#center,0.5,ayumi_seifuku_141.png,0
$voice,42400705
歩美「……、…ん？」
$k

大悟「（よしっ！）」
$k

作戦が見事に成功し、歩美ちゃんがゆっくり
とその目を上げる。
$k

$voice,42400805
歩美「え〜っと…、え〜〜〜っと……」
$k

#center,0.5,ayumi_seifuku_004.png,0
$voice,42400905
歩美「あ……」
$k

#center,0.5,ayumi_seifuku_003.png,0
若干周囲をきょろきょろとしながらも、すぐ
に状況を把握できたようで、彼女はきりりと
姿勢を正した。
$k

#center,0.5,kana_suit_001.png,0
$voice,42401002
華奈「それではこの例文を踏まえて、次の練
　習問題をやってみましょう」
$k

#center,0.5,kana_suit_002.png,0
$voice,42401102
華奈「今日は７月１０日ですから……、若槻
　さん、お願いします」
$k

// 華奈スーツ（左）。歩美制服（右）。
#character,0.5,kana_suit_001.png,erase,ayumi_seifuku_003.png,0
$voice,42401205
歩美「はい」
$k

大悟「ふぅ…、危なかった……」
$k

すっと起立する彼女の姿に、ほっと安堵の息
が漏れる。
$k

なぜ『７月１０日だから歩美ちゃん』なのか
は相変わらず謎だが、とりあえず最悪の事態
は避けられたようだ。
$k

#right,0.5,ayumi_seifuku_004.png,0
$voice,42401305
歩美「『彼はとても裕福なので、いつもブラ
　ンド物の服ばかり着ている』、…という意
　味だと思います」
$k

#left,0.5,kana_suit_022.png,0
$voice,42401402
華奈「はい、正解です。よくできましたぁ〜
　」
$k

歩美ちゃんの模範解答に対して、満面の笑み
で讃える華奈姉さん。やはり俺の選択は間違
っていなかった。
$k

#character,0.5,erase,same,erase,0

// 効果音：４６１２：携帯バイブレーション
$se,00004612,NO
（ぶるぶるっ、ぶるぶるっ）
$k

大悟「…ん？」
$k

歩美ちゃんが着席してすぐ、今度は俺の携帯
電話が震え出す。
$k

『ありがと　あゆみ』
$k

#center,0.5,ayumi_seifuku_021.png,0
$voice,42401505
歩美「うふっ♪」
$k

そちらに目を向けると、彼女のお茶目なウィ
ンク。それには胸ときめくものが感じられる
けど……。
$k

$next,486i_424i_3
