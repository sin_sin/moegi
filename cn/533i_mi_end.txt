// みちるシナリオスタッフロール。

// エンディングショートスタート。ループしない。
$bgm,26_s,NO
$wait,2.0

// 全面黒。
#background,1.0,black.jpg,same,0
$wait,1.0

// タイトルロゴ＋スタッフ表示。
#background,1.0,end_02_staff.jpg,same,0
$wait,7.0

// 通学路（昼）。１ブロックに１１秒
#background,1.0,tsuugakuro_hiru.jpg,same,0
$wait,1.0

// 企画・シナリオ。
#center,0.5,end_03_scenario_1.png,0
$wait,2.5

// 企画・シナリオ、ＤＥＥＺ
#center,0.5,end_04_scenario_2.png,0
$wait,4.0

// 全面白。
#background,1.0,white.jpg,same,0

// 商店街（昼）。
#background,1.0,shoutengai_hiru.jpg,same,0
$wait,1.0

// 原画
#center,0.5,end_05_genga_1.png,0
$wait,2.5

// 原画（よしむら、水原）
#center,0.5,end_06_genga_2.png,0
$wait,4.0

// 全面白。
#background,1.0,white.jpg,same,0

// 自宅の玄関（昼）
#background,1.0,jitaku_genkan_hiru.jpg,same,0
$wait,1.0

// 音楽
#center,0.5,end_07_music_1.png,0
$wait,2.5

// 音楽（ｂｌｕｅ）
#center,0.5,end_08_music_2.png,0
$wait,4.0

// 全面白。
#background,1.0,white.jpg,same,0

// キャスト。
#center,0.5,end_15_cast.png,0
$wait,2.5

// 全面白。
#background,1.0,white.jpg,same,0

// ６３０１：みちる、保健室で競馬新聞を読む＜視線が主人公へ＞）
#background,1.0,00006301.jpg,same,0
$cfinto,0,0
$wait,1.0

// 海堂みちる（右）。
#right,0.5,end_18_michiru_1.png,0
$wait,2.5

// 海堂みちる（２）（右）
#right,0.5,end_19_michiru_2.png,0
$wait,4.0

// 全面白。
#background,1.0,white.jpg,same,0

// 教室（昼）。
#background,1.0,gakuen_kyoushitsu_hiru.jpg,same,0
$wait,1.0

// 華奈スーツ（左）。まきいづみクレジット（右）。
#character,0.5,kana_suit_001.png,same,end_17_kana_2.png,0
$wait,4.0

// 立ち絵消す。
#character,0.5,erase,same,erase,0
$wait,0.5

// 優衣制服（左）。カンザキカナリクレジット（右）。
#character,0.5,yui_seifuku_003.png,same,end_21_yui_2.png,0
$wait,4.0

// 立ち絵消す。
#character,0.5,erase,same,erase,0
$wait,0.5

// 歩美制服（左）。春日アンクレジット（右）。
#character,0.5,ayumi_seifuku_001.png,same,end_23_ayumi_2.png,0
$wait,4.0

// 立ち絵消す。
#character,0.5,erase,same,erase,0
$wait,0.5

// 脇役キャスト（連名）。
#center,0.5,end_30_cast_renmei.png,0
$wait,4.0

// 全面白。
#background,1.0,white.jpg,same,0

// 学食（昼）。
#background,1.0,gakuen_gakushoku_hiru.jpg,same,0
$wait,1.0

// オープニング曲。
#center,0.5,end_40_opening_1.png,0
$wait,2.5

// オープニング曲（クレジット）。
#center,0.5,end_41_opening_2.png,0
$wait,4.0

// 立ち絵消す。
#character,0.5,same,erase,same,0
$wait,0.5

// エンディング曲。
#center,0.5,end_42_ending_1.png,0
$wait,2.5

// エンディング曲（クレジット）。
#center,0.5,end_43_ending_2.png,0
$wait,4.0

// 全面白。
#background,1.0,white.jpg,same,0

// 制作・著作。
#center,0.5,end_38_seisaku_1.png,0
$wait,2.5

// 制作・著作（Ｎａｉｌ）。
#center,0.5,end_39_seisaku_2.png,0
$wait,4.0

// 全面白。
#background,1.0,white.jpg,same,0

// ６３４０：みちる、エンディングショット（ノーマル）
#background,1.0,00006340.jpg,same,0
$cfinto,0,0

$wait,1.0

// ６３４１：みちる、エンディングショット（ハッピーエンド）
#background,1.0,00006341.jpg,same,0
$cfinto,24,1

// クリック待ち。
$k

// 全面黒。
#background,1.0,black.jpg,same,0

// 次のファイルへ。
$next,531i_0831_mi
