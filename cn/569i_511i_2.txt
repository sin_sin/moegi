// 選択肢１を選んだ場合。（ほっと安堵感を覚える。）
// みちる好感度（＋１）
$pfplus,30,1

大悟「…まったくいつも通りの台詞ですね、
　みちる先生」
$k

ほっと安堵の息を含めながら、俺はそう返し
た。
$k

#center,0.5,michiru_nemaki_004.png,0
$voice,51102503
みちる「お前だろ、皮肉代わりに言ってくる
　のは」
$k

大悟「あれ？　そうでしたっけ？」
$k

#center,0.5,michiru_nemaki_001.png,0
$voice,51102603
みちる「まあ、それだけ私をよく観察してい
　ると言うことか」
$k

大悟「…人をストーカー扱いしないでくださ
　い」
$k

#center,0.5,michiru_nemaki_002.png,0
$voice,51102703
みちる「構わん構わん。お前がストーキング
　をする分には、全然怖くないからな」
$k

#center,0.5,michiru_nemaki_001.png,0
$voice,51102803
みちる「むしろストーキングの極限にまで行
　き着いて、私の行動を先読みしてもらいた
　いくらいだ」
$k

大悟「それって、『召使い』ってことですか
　？」
$k

#center,0.5,michiru_nemaki_022.png,0
$voice,51102903
みちる「おおっ、よくわかっているじゃない
　か、青年！」
$k

#center,0.5,michiru_nemaki_001.png,0
$voice,51103003
みちる「私がコーヒーを飲みたいと思う前に
　コーヒーを入れ、競馬新聞を読みたいと思
　う前に競馬新聞を買ってくる」
$k

#center,0.5,michiru_nemaki_002.png,0
$voice,51103103
みちる「それがストーキングの極意というも
　のだっ！」
$k

大悟「また大いに語弊のあることを……」
$k

がっくりとうなだれてしまう気分だったが、
その力説はあまりにも彼女らしかった。
$k

$next,570i_511i_3
