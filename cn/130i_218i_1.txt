// 選択肢１を選んだ場合。（とりあえず『步美さん』と言う。）

大悟「……、…步美同学……」
$k

#center,0.5,ayumi_seifuku_003.png,0
$voice,20705505
步美「嗯？ 你说什么了么？」
$k

大悟「步美同学……」
$k

#center,0.5,ayumi_seifuku_002.png,0
$voice,20705605
步美「真遗憾，最后两个字没听见呢」
$k

大悟「小步美！」
$k

#center,0.5,ayumi_seifuku_022.png,0
$voice,20705705
步美「怎么了，大悟君？」
$k

大悟「……」
$k

脸上挂满笑容看着我的若槻同…不对，是小步美。
$k

晤哇〜…这可太让人害羞了点……。如果被什么认识的人看到的话，肯定会被当作笨蛋的吧……。
$k

#center,0.5,ayumi_seifuku_001.png,0
$voice,20705805
步美「想说的话把称谓去掉也没关系的哟？ 说『步美』好了」
$k

大悟「就这个还是饶了我吧……」
$k

#center,0.5,ayumi_seifuku_004.png,0
$voice,20705905
步美「哎呀，遗憾。如果能如愿的话，我也想毫无顾忌的叫你『小悟』的呢〜」
$k

大悟「…如果发生那种事的话，我肯定会被退学的……」
$k

#center,0.5,ayumi_seifuku_021.png,0
$voice,20706005
步美「欸呵呵♪」
$k

对小优来说虽然是件很令人高兴的事，不过对我的要求可是有点苛刻了就是了…。
$k

$next,132i_218i_3
