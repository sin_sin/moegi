// 207.txt
// 　　ファイルナンバー２０７　・・・　步美に優衣のことを説明する
// 　　　　　　登場　・・・　５步美
// 　　　　７月２日（月）　午後０時５０分　学食
// （アイキャッチなし）

// ＢＧＭスタート。
$bgm,17,YES

// 学食（昼）ワイプ。步美制服。
#background,1.0,gakuen_gakushoku_hiru.jpg,same,0
#center,0.5,ayumi_seifuku_001.png,0
$voice,20700005
步美「就在这里吧」
$k

大悟「嗯」
$k

排队期间若槻的心情总算是有所平静，我和她在空位上坐了下来。
$k

今天我们俩也是A餐的汉堡肉定食。确实是有着食堂菜肴风格分量十足的一餐。
$k

#center,0.5,ayumi_seifuku_004.png,0
$voice,20700105
步美「我们，被看作是情侣关系了么…？」
$k

若槻不经意间移开视线，害羞的嘀咕起来。
$k

#center,0.5,ayumi_seifuku_003.png,0
$voice,20700205
步美「因为听到她们说的那种事…我想说不定，旁人眼里是那样看的呀，什么的……」
$k

大悟「……」
$k

// 彼氏彼女の関係だと…？
$select,『能被这样认为的话也很开心呢』,217i_207i_2
$select,『怎么会〜那种程度的事情就……』,216i_207i_1
$select,（难得可以一起进餐，就算有些得寸进尺也……）,hint
$k
