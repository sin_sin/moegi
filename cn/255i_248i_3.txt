// 集結。

// 全面白ワイプ。
#background,1.0,white.jpg,same,0
// ＢＧＭスタート。
$bgm,09,YES

// 被服室（夜）ワイプ。優衣制服（左）。歩美制服（右）。
#background,1.0,gakuen_hifukushitsu_yoru.jpg,same,0
#character,0.5,yui_seifuku_003.png,same,ayumi_seifuku_001.png,0

大悟「へぇ〜、だんだんとできあがってきた
　ね」
$k

#right,0.5,ayumi_seifuku_021.png,0
$voice,24801805
歩美「そうね、予定通りに順調に進んでるわ
　」
$k

#left,0.5,yui_seifuku_001.png,0
優衣「……」
$k

作業台に衣装を広げて、今日の成果をほれぼ
れと見詰める。
$k

今日は基礎の部分を縫ったというところだろ
うか、まだ合わせていない箇所はあっても、
人の形はしっかりと見えていた。
$k

#character,0.5,erase,ayumi_seifuku_001.png,erase,0
$voice,24802005
歩美「あとはここをこうして、ここがこうな
　って…、こうかしら」
$k

説明書と見比べながら、軽く生地を折り込む
歩美ちゃん。そうすることにより、衣装の形
がさらに明確になる。
$k

大悟「これって、どんな人が着るんだろうね
　。早く見てみたいな」
$k

#center,0.5,ayumi_seifuku_021.png,0
$voice,24802105
歩美「うふふっ…、慌てなくたって七夕祭り
　の日に見れるわよ」
$k

大悟「厳正な審査の中から選ばれた、今年一
　番の美女…。何だか、夢が膨らむなぁ〜」
$k

#center,0.5,ayumi_seifuku_006.png,0
$voice,24802205
歩美「夢を膨らませるのはいいけど、鼻の下
　は伸ばさないでね」
$k

大悟「あいたたたっ！」
$k

茶目っ気ある表情と共に、彼女の指が俺の頬
をつねる。その感触は、大げさに声を出すよ
うなものではなかったが。
$k

// 優衣制服（左）。歩美制服（右）。
#character,0.5,yui_seifuku_003.png,erase,ayumi_seifuku_001.png,0
$voice,24802305
歩美「じゃあ、明日もよろしくね」
$k

大悟「は〜い」
$k

#left,0.5,yui_seifuku_169.png,0
優衣「（ぺこり）」
$k

そしてお互いに挨拶をして、今日の作業は終
了した。
$k

……。
$k

#hide,0.5

// ＢＧＭストップ。
$bgm,stop,YES

// 次のファイルへ。
$next,249i_0704_mi
