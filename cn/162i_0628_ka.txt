﻿// １６２　・・・　二人の帰り道　６月２８日（木）　午後５時　土手の歩道
#screen,1.0,eye_background.jpg,eye_day_0628.png,eye_time_pm06.png,eye_loc_yuuhodou.png,eye_min_00.png,0
$k

// ＢＧＭスタート。
$bgm,16,YES

// 土手の遊歩道（夜）フェード。
#background,1.0,yuuhodou_yoru.jpg,erase,0
#show,0.5

一边仰望被藏青色渲染的天空，我和华奈姐两
人边沿着八木川河堤的散步道缓步前行。
$k

从学校离开后暂时到商店街转了一会儿，然后
就沿着回家的路进发了。我的右手上，正提着
装着一大堆食材的购物袋。
$k

本来即便回家稍微会有些绕远，不过河岸边清
爽的微风拂过使这条路着实有被选择的价值。
$k

#center,0.5,kana_suit_003.png,0
$voice,16200002
华奈「小优，要是能开心就好了呢…」
$k

在保健室没有提及的事，现在正从华奈姐的口
中呢喃而出。
$k

#center,0.5,kana_suit_162.png,0
$voice,16200102
华奈「只有我一个人乐在其中，小优说不定完全
没有高兴起来呢……」
$k

大悟「……」
$k

…这句话的末尾我并没有听清。
$k

华奈姐和平常充满活力和自信相比有着很大的
差别。或许是内心感受到了一定的压力。
$k

从华奈姐的表情当中，深深的让我感到确实是如
此……。
$k

// （選択：個人授業を不安に思う華奈に…？）
$select,『不要担心哟』,103i_162i_1
$select, 『…我想只能坚持到底不是吗』,104i_162i_2
$select,（就用使人坚强的话语鼓励不安着的华奈吧）,hint
$k
