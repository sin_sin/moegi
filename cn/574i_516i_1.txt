// 選択肢１を選んだ場合。（…確かにそうかも。）
// みちる好感度（＋１）
$pfplus,30,1

#center,0.5,erase,0

…確かにそうかもしれないな。可能性は決し
て否定できない。
$k

性格や言動はともかく、容姿だけで考えれば
かなりのレベルに達しているのだから……。
$k

$next,576i_516i_3
