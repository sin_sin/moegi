// 前ファイルからの続き。

#left,0.5,michiru_inner_002.png,0
$voice,60806203
みちる「ま、責任がどうのこうのと言う問題
　ではありませんよ」
$k

#right,0.5,akane_inner_004.png,0
$voice,60806307
茜「えっ…？」
$k

…そんな雰囲気は好ましくないと思ったのか
、みちる先生は軽い口調で言った。
$k

#left,0.5,michiru_inner_001.png,0
$voice,60806403
みちる「慢性的な拒食症だったら厄介ですが
　、幸いにして優衣はこの家に来てからは食
　事を抜いたことがありません」
$k

$voice,60806503
みちる「朝晩は大悟が作ったものを食べ、昼
　は私の連れていく喫茶店でしっかり食べて
　いますので、ご安心を」
$k

#right,0.5,akane_inner_141.png,0
$voice,60806607
茜「…ですから、そうおっしゃると言うこと
　は、やはり私に責任が……」
$k

$voice,60806703
みちる「優衣よりも先にあなたが参ってしま
　わないように」
$k

#right,0.5,akane_inner_004.png,0
$voice,60806807
茜「はい…？」
$k

みちる先生はおもむろに体を前に出し、茜さ
んの手に自らの手をそっと重ねた。
$k

#left,0.5,michiru_inner_002.png,0
$voice,60806903
みちる「カウンセリングをしているとよくあ
　るんですよ、当の本人よりも周囲が神経質
　（ナーバス）になってしまうことが」
$k

#right,0.5,akane_inner_141.png,0
$voice,60807007
茜「……」
$k

#left,0.5,michiru_inner_021.png,0
$voice,60807103
みちる「多少は長い目で見ることも必要です
　。どうぞあなたは、大らかな気持ちで優衣
　に接してあげてください」
$k

#right,0.5,akane_inner_169.png,0
$voice,60807207
茜「……」
$k

#right,0.5,akane_inner_001.png,0
$voice,60807307
茜「…はい」
$k

ほんの僅かではあるが、その表情に安堵感が
浮かぶ。
$k

みちる先生はすでに見抜いていたのだろう、
茜さんの心の中に焦りと自責の念があったこ
とに。
$k

#left,0.5,michiru_inner_002.png,0
$voice,60807403
みちる「まあ、家事をしてくれることは私達
　にとっては大変ありがたいことではありま
　すが」
$k

#character,0.5,erase,michiru_inner_001.png,erase,0
…そこまで言って、みちる先生の目は俺のほ
うへと向いた。
$k

#center,0.5,michiru_inner_002.png,0
$voice,60807503
みちる「昨日、福引きで当てた肉があっただ
　ろ。今夜はそれを焼いてもらおう」
$k

大悟「いいですね、それ。超高級和牛ですか
　ら、すごくおいしいと思いますよ」
$k

大悟「…あ、でも、確か四人分しかなかった
　んですけど」
$k

#center,0.5,michiru_inner_005.png,0
$voice,60807603
みちる「お前は抜きだ」
$k

大悟「何言ってるんですか、みちる先生っ！
　俺だってあの肉を食べたいですよ！」
$k

#center,0.5,michiru_inner_006.png,0
$voice,60807703
みちる「黙れ、お前は何も当てられなかった
　じゃないか。この家には、稼ぎのない奴に
　食わせるメシはないぞ」
$k

大悟「…ハズレばっかりだったのは事実です
　けど、それじゃあ優衣ちゃん以外は誰も食
　べられませんよ」
$k

#center,0.5,michiru_inner_005.png,0
$voice,60807803
みちる「私は福引き券を提供した」
$k

$voice,60807903
みちる「華奈は自分の福引き券を優衣に与え
　、それを使って優衣は肉を当てた」
$k

$voice,60808003
みちる「お前は…、言うまでもないな」
$k

大悟「…う」
$k

#center,0.5,michiru_inner_006.png,0
$voice,60808103
みちる「誠に残念なことではあるが、お前に
　肉を食う資格はない」
$k

$voice,60808203
みちる「私達が優雅に肉の味を堪能している
　横で、さびしくたくあんでもかじっている
　が良かろう」
$k

大悟「そんなぁ〜〜〜っ！」
$k

#center,0.5,akane_inner_141.png,0
$voice,60808307
茜「あ…、あの、私は要りませんから」
$k

俺とみちる先生のやり取りの内容を理解でき
たのだろう、茜さんは遠慮がちに口を挟んで
きた。
$k

$voice,60808407
茜「お肉が四人分しかないのでしたら、私は
　食べませんので、二宮さんが召し上がって
　ください」
$k

大悟「え…、え〜っと…、そういう意味で言
　っていたんじゃなくて……」
$k

#center,0.5,akane_inner_026.png,0
$voice,60808507
茜「いいんですいいんです、私はみなさんに
　お世話になっている身なんですから」
$k

大悟「困ったな……」
$k

つい先ほど、彼女は『神経質になるな』とみ
ちる先生から言われたはず。
$k

それは優衣ちゃんだけではなく、しばらくは
一緒に生活する俺達に対してのことでもある
はずなのだが……。
$k

#center,0.5,michiru_inner_002.png,0
$voice,60808603
みちる「ほれ、大悟」
$k

大悟「はい？」
$k

どうしようかと迷っている最中に、不意に俺
の目の前に差し出されたみちる先生の手の平
。そして、その上に乗っているお金。
$k

$voice,60808703
みちる「もう一人前買ってこい。それと付け
　合わせも必要になるだろ」
$k

大悟「みちる先生……」
$k

その優しさがじんわりと心に伝わってくる。
$k

今までのふざけたやり取りも、茜さんが気ま
ずくならないための配慮だったのだろうか。
さらに自腹まで切ってくれるなんて…。
$k

大悟「…って、十円玉が一枚しかないんです
　けど」
$k

#center,0.5,michiru_inner_001.png,0
$voice,60808803
みちる「それで足りるよな？　七夕祭りの期
　間だし」
$k

大悟「七夕祭りでもクリスマスでも、まさか
　十円で肉が買えるなんてことは……」
$k

$voice,60808903
みちる「足りるよな？」
$k

大悟「……」
$k

一見するとにこやかなみちる先生の目も、そ
の奥には悪魔のような冷酷さが宿っていた。
$k

大悟「足りると、思います……」
$k

#center,0.5,michiru_inner_024.png,0
$voice,60809003
みちる「よぉ〜っし、よくぞ言った、青年！
　その金で高級牛肉でも何でも、好きなもの
　を買ってこい！」
$k

大悟「もうメチャクチャです、みちる先生…
　…」
$k

心の中でさめざめと流れ落ちる涙。ほんの一
瞬でもこの人が優しいなどと思ってしまった
ことが大いに悔やまれる。
$k

#center,0.5,akane_inner_141.png,0
$voice,60809107
茜「本当に私の分は結構なんですけど……」
$k

大悟「ああっ、そんなこと言わないでくださ
　いよ！」
$k

…ふと気が付けば、茜さんはとても申し訳な
さそうな顔をしていた。
$k

大悟「作ってくれる人が食べられなかったら
　困るじゃないですか！　遠慮なんてしない
　でください！」
$k

#center,0.5,akane_inner_162.png,0
$voice,60809207
茜「でも……」
$k

大悟「そ…、そうだ、今から一緒に買い物に
　行きましょう！　もしかしたら、本当に十
　円玉一枚で足りるかもしれません！」
$k

#center,0.5,akane_inner_004.png,0
$voice,60809307
茜「……」
$k

#center,0.5,akane_inner_021.png,0
$voice,60809407
茜「…はい」
$k

小さく浮かぶ彼女の笑み。自分でもあり得な
いこととは思っているが、それでも冗談と捉
えてくれれば本望だ。
$k

// みちる室内着（左）。茜室内着（右）。
#character,0.5,michiru_inner_001.png,erase,akane_inner_001.png,0
$voice,60809503
みちる「つりは返せよ」
$k

大悟「…どうしてそんな言葉を平然と言える
　んですか、みちる先生は」
$k

多少はやわらかくなった空気の中に於いても
、彼女の非道さは一向に変わらなかった。
$k

$voice,60809603
みちる「街は祭りで賑やかになっていると思
　いますから、ゆっくりと楽しんできてくだ
　さい」
$k

#right,0.5,akane_inner_169.png,0
$voice,60809707
茜「ありがとうございます」
$k

#left,0.5,michiru_inner_002.png,0
$voice,60809803
みちる「それから、帰ってきたら部屋のほう
　にも案内しましょう」
$k

大悟「…あ、そのことなんですけど」
$k

#character,0.5,michiru_inner_003.png,same,akane_inner_003.png,0
みちる先生の言葉で思い出し、俺は口を挟ん
だ。
$k

大悟「実はもう空き部屋がないんです。だか
　ら誰かと相部屋になるか、居間で寝てもら
　うしかありません」
$k

#left,0.5,michiru_inner_004.png,0
$voice,60809903
みちる「何だ、意外と狭いな、この家も」
$k

大悟「居候がたくさんいるからですよっ！」
$k

#left,0.5,michiru_inner_001.png,0
$voice,60810003
みちる「もし差し支えなければ、私と相部屋
　になりましょう。多少散らかっていますが
　、夕方までには片付けますので」
$k

#right,0.5,akane_inner_169.png,0
$voice,60810107
茜「はい、よろしくお願いします」
$k

#character,0.5,erase,same,erase,0
…珍しくマトモな提案をするみちる先生だっ
たが、その意図はすぐに分かった。
$k

まず第一に、俺と優衣ちゃんの部屋のある二
階は良くない。彼女と優衣ちゃんが余計な気
を遣う心配があるからだ。
$k

そして第二に、華奈姉さんの部屋は言わずも
がな。あの寝起きの悪さは、茜さんに迷惑が
掛かってしまうだろう。
$k

無論、女性を居間や書斎に寝かせるわけにも
行かない。そんなことをさせてしまったら、
常識が疑われてしまうだろう。
$k

…とすれば、残すはみちる先生の部屋のみ。
$k

こうすることで茜さんも、みちる先生がそば
にいるという安心感を得ることができるし、
$k

…何より、彼女が一人で思い詰めることもな
くなる。
$k

先ほどの『周囲が神経質にならないように』
という言葉をより実践しやすくなると言うも
のだ。
$k

#center,0.5,akane_inner_001.png,0
大悟「それじゃあ、買い物に行きましょうか
　」
$k

話が適当にまとまったところで、彼女を促す
。
$k

#center,0.5,akane_inner_021.png,0
$voice,60810207
茜「はい、行きましょう」
$k

快く頷く茜さん。その声も顔色も、先ほどま
でに比べて少し軽くなっているようだ。
$k

#center,0.5,michiru_inner_001.png,0
$voice,60810303
みちる「私の金だからって無駄遣いするんじ
　ゃないぞ、青年」
$k

大悟「…そういう台詞は、無駄遣いできるほ
　どのお金を用意してから言ってくださいね
　」
$k

茜さんに対しては優しさや気配りを見せても
、俺に対してはやっぱりこんな調子だった。
$k

……。
$k

#hide,0.5

// ＢＧＭストップ。
$bgm,stop,YES

// 次のファイルへ。
$next,609i_0708_aka
