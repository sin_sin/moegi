// １７１　・・・　覚えてないよね？　６月２９日（金）　午前８時１５分　学校廊下
#screen,1.0,eye_background.jpg,eye_day_0629.png,eye_time_am08.png,eye_loc_gakuen_rouka.png,eye_min_15.png,0
$k

// ＢＧＭスタート。
$bgm,03,YES

// 学園廊下（昼）。華奈スーツ。
#background,1.0,gakuen_rouka_hiru.jpg,erase,0
#center,0.5,kana_suit_001.png,0
#show,0.5

$voice,17100002
华奈「到达〜〜〜」
$k

大悟「呼…」
$k

到了又一天的6月29日周五。今天也是经历了
巨大的磨难，总算是想到办法让华奈姐在迟到
前到达了。
$k

$voice,17100102
华奈「今天也是非常从容呢，小悟♪」
$k

大悟「…时间上是有些富裕，不过精神上已经
快要崩溃了啦」
$k

#center,0.5,kana_suit_021.png,0
我只能发出阵阵叹息，而华奈姐却是满面春风
的压过来把手表上的时间秀在我的眼前。
$k

从旁人眼光看，这简直就是感情很好的两人互
相嬉闹一样，在这种场合下果然还是有些危险
……。
$k

$next,111i_171i_3
