// 集結。

$voice,60201503
みちる「何だ、お前は見られたい性分だった
　のか」
$k

大悟「……」
$k

どういう話が向こうで進んでいたんだ、いっ
たい……。
$k

$voice,60201603
みちる「そのような趣味の人間がいることは
　理解しているが、今は華奈と優衣が一緒だ
　からなあ」
$k

$voice,60201703
みちる「…あ、私はいいんだぞ、私は。お前
　がそれを望むのなら、私は喜んで協力及び
　指導をしてやろう」
$k

大悟「そういうわけじゃなくって！」
$k

すぐに話の腰を折りたがる彼女に対して、も
う一度気を引き締める。
$k

大悟「今、ここにメイドさんがいるんです。
　そちらだとちょっと騒がしすぎると思いま
　すから、こっちに来てくれませんか？」
$k

$voice,60201803
みちる「……」
$k

$voice,60201903
みちる「…華奈と優衣も連れていくのか？」
$k

瞬時に真面目なものへと切り替わる彼女の声
。ことの重大さは十分に理解しているようだ
。
$k

大悟「お願いします」
$k

$voice,60202003
みちる「わかった、すぐに行こう」
$k

それから俺は手早く今の場所を説明した。
$k

あそこからは十分ほど掛かるだろうか。現在
の人込みの様子と、女性三人の歩く速さも考
えなければならないが。
$k

大悟「もしわからなくなったら、もう一度電
　話してください」
$k

$voice,60202103
みちる「りょ〜かい」
$k

……。
$k

// ＢＧＭストップ。
$bgm,stop,YES

大悟「ふぅ……」
$k

携帯電話をポケットにしまい、小さく息をつ
く。
$k

本来ならば、こんな形で優衣ちゃんとメイド
さんを会わせたくはなかったけれど……。
$k

……。
$k

#hide,0.5

// 次のファイルへ。
$next,603i_0707_mi
