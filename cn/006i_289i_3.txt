
$voice,28900002
？？「だいちゃん、だいちゃんっ！」
$k

大悟「ん？」
$k

玄関からドタドタと騒がしい足音と共に、喜
々とした声が聞こえてくる。
$k

#center,0.5,kana_inner_002.png,0
$voice,28900102
華奈「いいでしょ、いいでしょ〜っ？　お母
　さんが新しい浴衣を買ってくれたのぉ〜！
　」
$k

新品の浴衣を持って居間に飛び込んでくる華
奈姉さん。
$k

夕食後に今井のおばさんから電話があり、彼
女は一度実家へ行っていたのだ。
$k

#center,0.5,kana_inner_022.png,0
$voice,28900202
華奈「どう、どう？　似合う？」
$k

華奈姉さんはなおも興奮が冷めやらない様子
で、浴衣を体に合わせる。
$k

#center,0.5,kana_inner_002.png,0
$voice,28900302
華奈「あぁ〜、うれしいなぁ〜、こんな綺麗
　な浴衣を着られて〜〜〜」
$k

#center,0.5,kana_inner_021.png,0
$voice,28900402
華奈「お母さんがね、『華奈はもう大人なん
　だから、いつまでも安物の浴衣を着てちゃ
　駄目よ』って言うの」
$k

#center,0.5,kana_inner_022.png,0
$voice,28900502
華奈「ほらほら、昔着てたのとは全然違うで
　しょ？　こういうの、『大人の魅力』って
　言うんだよぉ〜？」
$k

大悟「は…、はは、そうかもね」
$k

満面の笑みを浮かべて、浴衣を愛でる華奈姉
さん。
$k

だけど…、どう考えても『大人の魅力』なん
ていう言葉は、今の華奈姉さんには似合わな
いな。
$k

この浴衣には、今井のおばさんの『願望』が
含まれているのだろう、きっと。
$k

#center,0.5,kana_inner_001.png,0
$voice,28900602
華奈「これで明日の準備はばっちりだね」
$k

思う存分愛でたところで、華奈姉さんは浴衣
を一度丁寧にたたんだ。
$k

大悟「どうせ着付けは今井のおばさんにやっ
　てもらうんだから、家に置いてくれば良か
　ったのに」
$k

#center,0.5,kana_inner_005.png,0
$voice,28900702
華奈「いいのっ、今日はこの浴衣さんと一緒
　に寝るのっ！」
$k

大悟「……」
$k

…この辺りも、華奈姉さんが『大人の魅力』
とは程遠い部分だった。
$k

#center,0.5,kana_inner_004.png,0
$voice,28900802
華奈「あ…、そうだ」
$k

大悟「ん？」
$k

#center,0.5,kana_inner_001.png,0
$voice,28900902
華奈「優衣ちゃんの着付けもお母さんにやっ
　てもらわないといけないから、その時間も
　考えたほうがいいね」
$k

大悟「…そうか、優衣ちゃんにも浴衣があっ
　たんだ」
$k

彼女の荷物の中に入っていた華麗な浴衣。
$k

それは神山家のメイドさんが用意してくれた
ものだが、一瞬だけでも優衣ちゃんのその姿
を彼女に見せてあげたいものだ。
$k

#center,0.5,kana_inner_004.png,0
$voice,28901002
華奈「みっちゃんはどうなのかなあ？　浴衣
　、あるのかなあ？」
$k

大悟「たぶん、ないと思うよ」
$k

#center,0.5,kana_inner_003.png,0
$voice,28901102
華奈「どうして？　みっちゃんに聞いたの？
　」
$k

大悟「だって……」
$k

…華奈姉さんが『大人の魅力』という言葉と
は無縁であるのと同じように、
$k

みちる先生は『日本的な情緒』とは対局に位
置する存在だから。
$k

#center,0.5,erase,0

// 効果音：６８０１：ぽかっ
$se,00006801,NO
（ぽかっ）
$k

大悟「あいたっ！？」
$k

#center,0.5,michiru_inner_005.png,0
$voice,28901203
みちる「なかなかおもしろいことを考えてい
　るなあ、お前は」
$k

大悟「人の心を覗かないでくださいよっ！」
　
$k

振り向くと、いきなり背後にみちる先生が立
っていた。
$k

しかもその頬が僅かに引きつっている。どう
やら自分の悪口には、鋭い感覚で即座に反応
するようだ。
$k

// みちる室内着（左）。華奈室内着（右）。
#character,0.5,michiru_inner_005.png,erase,kana_inner_001.png,0
$voice,28901302
華奈「みっちゃんは、浴衣あるの〜？」
$k

そんな俺とみちる先生のやり取りにも自分の
ペースを崩さず、華奈姉さんが問い掛ける。
$k

#left,0.5,michiru_inner_004.png,0
$voice,28901403
みちる「いや、持ってない」
$k

大悟「…やっぱり」
$k

#left,0.5,michiru_inner_005.png,0
$voice,28901503
みちる「どういう意味の『やっぱり』か、口
　に出して説明してもらおうか、大悟君？」
$k

大悟「ううっ……」
$k

その不敵な笑みは、俺を黙らせるのに十分だ
った。
$k

#right,0.5,kana_inner_002.png,0
$voice,28901602
華奈「それじゃあ、私が前に着てたのにする
　？　実家にまだ残してあると思ったけど」
$k

#left,0.5,michiru_inner_004.png,0
$voice,28901703
みちる「う〜ん、華奈の浴衣では身長が厳し
　そうだな。私と華奈とでは、今でも１０セ
　ンチは差がある」
$k

#right,0.5,kana_inner_003.png,0
$voice,28901802
華奈「あ…、そっかあ」
$k

#left,0.5,michiru_inner_001.png,0
$voice,28901903
みちる「まあいいさ、普段の服で。別に浴衣
　でなければ立ち入りが禁止されるわけでも
　あるまい」
$k

#right,0.5,kana_inner_162.png,0
$voice,28902002
華奈「はぅ……」
$k

あっさりとしたみちる先生とは逆に、小さく
ため息をついて残念がる華奈姉さん。その気
持ちはわからないでもないけど。
$k

#character,0.5,erase,kana_inner_001.png,erase,0
$voice,28902102
華奈「だいちゃんはどうするの？」
$k

大悟「俺？」
$k

気を取り直して、華奈姉さんの目が今度はこ
ちらに移る。
$k

// 歩美ルートかその他ルートかで分かれる。
$next,289i_ayumi_3

