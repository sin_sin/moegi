// 703.txt
// 　　ファイルナンバー７０３　・・・　短冊に願いを書く
// 　　　　　　登場　・・・　２華奈
// 　　　　７月７日（土）　午後６時３０分　土手の遊歩道
#screen,1.0,eye_background.jpg,eye_day_0707.png,eye_time_pm06.png,eye_loc_yuuhodou.png,eye_min_30.png,0
$k

// ＢＧＭスタート。
$bgm,17,YES

// 土手の遊歩道（祭り明るい）。華奈浴衣。
#background,1.0,dote_matsuri_1.jpg,erase,0
#center,0.5,kana_yukata_022.png,0
#show,0.5

$voice,70300002
華奈「うわぁ〜、きれ〜〜〜いっ！」
$k

大悟「ふえ〜〜〜」
$k

土手の遊歩道は、すでに大勢の見物客と無数
の七夕飾りに埋め尽くされていた。
$k

夜に舞う色とりどりの短冊と、そのさらに上
にある輝く星達。その光景にはいつも神秘的
なものを感じてしまう。
$k

#center,0.5,kana_yukata_001.png,0
$voice,70300102
華奈「はい、だいちゃん」
$k

大悟「あ…、うん」
$k

さっそく手近な短冊を手に取り、俺の前に差
し出してくる華奈姉さん。
$k

$voice,70300202
華奈「だいちゃんは何をお願いするの？」
$k

大悟「う〜ん、そうだなあ……」
$k

『やっぱり、華奈姉さんといつまでも一緒に
いたいってこと、…かな』
$k

…な〜んて言ったら思いっ切り引かれちゃう
だろうな。話があまりにも飛びすぎていて。
$k

大悟「華奈姉さんのほうから教えてよ」
$k

照れ隠しにそう言いながら、彼女の手にある
短冊を覗き込むと。
$k

#center,0.5,kana_yukata_002.png,0
$voice,70300302
華奈「私は…、これ」
$k

大悟「ん？」
$k

#center,0.5,kana_yukata_001.png,0
$voice,70300402
華奈「『日本に住む人みんなが、英語を話せ
　るようになりますように』」
$k

大悟「……」
$k

壮大と言うか…、あまりにも現実から離れす
ぎているぞ、この夢は……。
$k

英語教師なら誰でも思うことなのだろうか…
。それでも、これを叶えるためには奇跡にも
匹敵する出来事が必要だぞ。
$k

#center,0.5,kana_yukata_022.png,0
$voice,70300502
華奈「良いお願いでしょ？」
$k

思わず固まってしまった俺に対して、華奈姉
さんはいかにも満足げな笑顔。
$k

大悟「も…、もう少し、身近なことのほうが
　いいんじゃないのかな…？」
$k

#center,0.5,kana_yukata_004.png,0
$voice,70300602
華奈「『身近』…？」
$k

#center,0.5,kana_yukata_003.png,0
$voice,70300702
華奈「それじゃあ、『青葉学園に通う人みん
　な』にする？」
$k

大悟「うん…、それなら華奈姉さんの努力次
　第で何とかなるかもしれない」
$k

…無論、『話せる』というレベルに達するに
はかなり厳しいと思うけど。
$k

#center,0.5,kana_yukata_021.png,0
$voice,70300802
華奈「そうだね〜、まずは小さなところから
　コツコツやったほうがいいよね」
$k

華奈姉さんは俺の心配など構わず、納得した
ようにうむうむと頷いていた。
$k

#center,0.5,kana_yukata_001.png,0
$voice,70300902
華奈「『青葉学園に通う人みんなが、英語を
　話せるようになりますように』」
$k

#center,0.5,kana_yukata_022.png,0
$voice,70301002
華奈「…これで良し、っと」
$k

そして再び満足げな表情を浮かべて、短冊を
竹にしっかりと結び付ける。
$k

ここまで来たら何も言わず、華奈姉さんの願
いが叶うことを祈るしかないだろう。その成
功確率は別として。
$k

#center,0.5,kana_yukata_001.png,0
$voice,70301102
華奈「はい、次はだいちゃんの番だよ？」
$k

大悟「う…、うん……」
$k

あんなにスケールの大きな願いを見せられて
は、自分は大いに迷ってしまうところだけれ
ど、
$k

さて、何にしようか……。
$k

// 七夕に願うことは…？
$select,『来年もまた、華奈姉さんと一緒に……』,763i_703i_1
$select,『学力が向上しますように』,764i_703i_2
$select,（二人で祭りに来たのですから、お願いすることは決まっています）,hint
$k
