// 434.txt
// 　　ファイルナンバー４３４　・・・　７月１５日歩美が遊びに来る
// 　　　　　　登場　・・・　５歩美
// 　　　　７月１５日（日）　午後１時００分　自宅の居間
#screen,1.0,eye_background.jpg,eye_day_0715.png,eye_time_pm01.png,eye_loc_jitaku_ima.png,eye_min_00.png,0
$k

// 日付設定。
$pfinto,1,7
$pfinto,2,15

// ＢＧＭスタート。
$bgm,17,YES

// 自宅の居間（昼）。歩美外出着。
#background,1.0,jitaku_ima_hiru.jpg,erase,0
#center,0.5,ayumi_outer_002.png,0
#show,0.5

$voice,43400005
歩美「こんにちは、大悟君」
$k

大悟「いらっしゃい、歩美ちゃん」
$k

#center,0.5,ayumi_outer_022.png,0
$voice,43400105
歩美「ああっ…、何だか懐かしく感じるわ、
　この家が。私、ここで一週間も生活してた
　のね……」
$k

大悟「…そんなこと言うのは早いよ、昨日帰
　ったばかりなのに」
$k

#center,0.5,ayumi_outer_001.png,0
$voice,43400205
歩美「それだけ印象深かった、ってことよ。
　楽しい思い出に時間の概念は関係ないわ」
$k

…時が進んで日曜日の昼下がり。うちに遊び
に来た歩美ちゃんは感慨深げに部屋の中を見
渡した。
$k

彼女が自宅に帰ったのが昨日の午後。あれか
ら約２４時間で舞い戻ってきたのだから、な
かなか忙しいものだ。
$k

#center,0.5,ayumi_outer_004.png,0
$voice,43400305
歩美「海堂先生達はいないの？」
$k

大悟「あぁ…、みんなで競馬に行ったよ。片
　平町の場外馬券売場までね」
$k

#center,0.5,ayumi_outer_003.png,0
$voice,43400405
歩美「ふぅ〜ん、海堂先生と華奈先生って競
　馬するんだぁ〜……」
$k

#center,0.5,ayumi_outer_004.png,0
$voice,43400505
歩美「…って、優衣ちゃんは駄目でしょう？
　」
$k

大悟「まあ、買うのはみちる先生だから。そ
　の辺はうまくやってるんだよ、きっと」
$k

#center,0.5,ayumi_outer_003.png,0
$voice,43400605
歩美「……」
$k

#center,0.5,ayumi_outer_162.png,0
$voice,43400705
歩美「…反論できないわね、その言葉には」
$k

歩美ちゃんもこの一週間で、みちる先生の性
格はよく理解している。だからこそ、それ以
上のことは何も言わなかった。
$k

#center,0.5,ayumi_outer_002.png,0
$voice,43400805
歩美「あ、ケーキ焼いてきたから。帰ってき
　たら、みんなで食べてね」
$k

大悟「ありがとう」
$k

#center,0.5,ayumi_outer_001.png,0
競馬のことはともかくとして、彼女はご機嫌
な調子に戻って、その手にあるペーパーボッ
クスを冷蔵庫の中に入れた。
$k

#center,0.5,ayumi_outer_004.png,0
$voice,43400905
歩美「でも…、そっかあ、みんな、今日はい
　ないんだぁ……」
$k

大悟「うん、そうだよ。帰りは夜になるとも
　言ってたけどね」
$k

#center,0.5,ayumi_outer_001.png,0
$voice,43401005
歩美「…あなたの部屋、行っていい？」
$k

// 効果音：３２００：心臓鼓動音（１回）
$se,00003200,NO
大悟「（どきっ）」
$k

恥ずかしげに、頬をほんのりと染めて言う彼
女の言葉に、思わず胸が高鳴ってしまった。
$k

#center,0.5,ayumi_outer_026.png,0
$voice,43401105
歩美「あ…、ご、誤解しないでね。私、ちょ
　っと忘れ物しちゃっただけだから」
$k

大悟「何だ、忘れ物か……」
$k

#center,0.5,ayumi_outer_002.png,0
$voice,43401205
歩美「ほら、早く行きましょ」
$k

大悟「はいはい」
$k

ほっと安堵するべきか、残念に思うべきか。
$k

そんな複雑な心境になりながらも、歩美ちゃ
んに背を押されるように二階へと向かった。
$k

// 自分の部屋（昼）ワイプ。歩美外出着。
#background,1.0,jibun_heya_hiru.jpg,same,0
#center,0.5,ayumi_outer_002.png,0
$voice,43401305
歩美「そっか〜、昼間のあなたの部屋って、
　こんな感じなんだぁ〜」
$k

大悟「えっ？　何かおかしい？」
$k

#center,0.5,ayumi_outer_001.png,0
$voice,43401405
歩美「ううん、そういう意味じゃなくて。結
　局、この部屋を使ったのは夜寝る時だけだ
　ったから」
$k

大悟「あぁ…、昼間は学校だったもんね」
$k

見慣れたものでも、光の具合で変わって見え
ることもある。彼女にとって、今の光景は新
鮮に感じられることだろう。
$k

大悟「…で、忘れ物って？」
$k

ざっと部屋の中を見渡しつつ、歩美ちゃんに
そう問い掛けると。
$k

#center,0.5,ayumi_outer_004.png,0
$voice,43401505
歩美「正確に言うと『忘れ物』じゃなくって
　、『忘れていたこと』なの」
$k

大悟「『忘れていたこと』…？」
$k

#center,0.5,ayumi_outer_169.png,0
$voice,43401605
歩美「もっと正確に言うと、『聞き忘れてい
　たこと』……、かな？」
$k

大悟「…？？」
$k

その意味が全くわからず、ついつい首を大き
く傾げてしまうが……、
$k

#center,0.5,ayumi_outer_003.png,0
$voice,43401705
歩美「大悟君」
$k

…彼女はそんな俺に構わず、目の前に正対し
た。
$k

$voice,43401805
歩美「私、あなたから大切な言葉を聞くこと
　を忘れてたわ」
$k

大悟「……」
$k

$voice,43401905
歩美「私はちゃんと言ったけど、あなたはま
　だ言ってないはずよ？」
$k

大悟「そ…、そうだったかな…？　確か…、
　言ったような気がするけど……」
$k

#center,0.5,ayumi_outer_006.png,0
$voice,43402005
歩美「ごまかさないのーっ！」
$k

大悟「は…、はいっ……」
$k

#center,0.5,ayumi_outer_005.png,0
ぷんすかとしながら、ぐいっと迫ってくる歩
美ちゃんの顔、目、そして唇。
$k

彼女が何を言っているのか、そして何を要求
しているのか……、ここまで来たら明白だっ
た。
$k

#center,0.5,ayumi_outer_006.png,0
$voice,43402105
歩美「はい、制限時間は三秒です。三秒以内
　に言わなかったら罰ゲームです」
$k

#center,0.5,ayumi_outer_005.png,0
$voice,43402205
歩美「さ〜〜〜っん、にい〜〜〜っ、い〜〜
　〜っち……」
$k

大悟「ああっ、言うよ言うよっ！」
$k

無情にもカウントを進める彼女の肩を素早く
掴み、
$k

そして、一度大きく深呼吸をして……。
$k

大悟「…好きだよ、歩美ちゃん」
$k

#center,0.5,ayumi_outer_001.png,0
$voice,43402305
歩美「……」
$k

#center,0.5,ayumi_outer_022.png,0
$voice,43402405
歩美「正解っ♪」
$k

#hide,0.5
$wait,1.0

// ６９６０：歩美、キスをする
#background,1.0,00006960.jpg,same,0
$cfinto,30,1

$wait,1.0

// ＢＧＭストップ。
$bgm,stop,YES
$wait,1.0

// 次のファイルへ。
$next,435i_ayu_end
